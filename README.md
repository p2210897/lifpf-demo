# UE LIFPF INF2030L Programmation Fonctionnelle

## Semestre 2023 Printemps

- [Configuration pour la salle de TP](CONFIGURATION.md)
- [Rappels git et gitlab](gitlab.md)
- [Questions sur le cours](https://forge.univ-lyon1.fr/programmation-fonctionnelle/lifpf/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=Question)

| jour  | heure         | type     | supports / remarques                                                                      |
| ----- | ------------- | -------- | ----------------------------------------------------------------------------------------- |
| 16/01 | 8h            | CM       | [Diapositives](cm/lifpf-cm1.pdf)                                                          |
|       | 9h45          | TD       | [Sujet](td/lifpf-td1-enonce.pdf), [corrigé](td/lifpf-td1-correction.pdf)                  |
| 23/01 | 8h            | CM       | [Diapositives](cm/lifpf-cm2.pdf), [Script démos](cm/cm2-demo.md)                          |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp1.md), [corrigé](tp/tp1.ml) <br> Groupe de TP, horaire et salle sur [tomuss] |
| 30/01 | 8h            | TD + QCM | [Sujet](td/lifpf-td2-enonce.pdf), [corrigé](td/lifpf-td2-correction.pdf)                  |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp2.md), [corrigé](tp/tp2.ml) <br> Groupe de TP, horaire et salle sur [tomuss] |
| 20/02 | 8h            | CM       | [Diapositives](cm/lifpf-cm3.pdf)                                                          |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp3.md), [corrigé](tp/tp3.ml)                                                  |
| 27/02 | 8h            | TD + QCM | [Sujet](td/lifpf-td3-enonce.pdf), [corrigé](td/lifpf-td3-correction.pdf)                  |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp4.md), [corrigé](tp/tp4.ml)                                                  |
| 06/03 | 8h            | CM       | [Diapositives](cm/lifpf-cm4.pdf)                                                          |
|       | 9h45          | TD + QCM | [Sujet](td/lifpf-td4-enonce.pdf), [corrigé](td/lifpf-td4-correction.pdf)                  |
| 13/03 | 8h            | CM       | [Diapositives](cm/lifpf-cm5.pdf)                                                          |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp5.md), [corrigé de l'exercice 1](tp/tp5.ml)                                  |
| 20/03 | 8h            | TD + QCM | [Sujet](td/lifpf-td5-enonce.pdf)                                                          |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp6.md)                                                                        |
| 27/03 | 9h45 ou 11h15 | TP       | [Sujet](tp/tp7.md) <br> Groupe de TP, horaire et salle sur [tomuss]                       |

###### Évaluation

- 5 QCMs en TD: 60 %
- ECA: 40%

Selon l'avancement du cours, il est possible qu'il y ait un TP noté lors de la dernière séance. Dans ce cas, il comptera dans les 60% des QCMs

[tomuss]: https://tomuss.univ-lyon1.fr
